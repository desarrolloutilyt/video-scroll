const path = require("path")

process.env.NODE_ENV = 'development';

const babelLoader = {
  loader: "babel-loader",
  options: {
    cacheDirectory: true
  }
};

const CSSLoader = {
  loader: "css-loader",
  options: {
    modules: {
      localIdentName: "[name]__[local]"
    }
  }
};

const postCSSLoader = {
  loader: "postcss-loader",
  options: {
    ident: "postcss"
  }
};

module.exports = {
  entry: {
    index: "./src/app/index.js"
  },
  output: {
    filename: "[name].dev.js",
    path: __dirname + "/public"
  },
  module: {
    rules: [
      {
        oneOf: [
          {
            test: /\.(js|jsx|mjs)$/,
            exclude: /node_modules/,
            use: [babelLoader]
          },
          {
            test: /\.scss$/,
            exclude: /\.module\.scss$/,
            use: [{
              loader: 'style-loader',
              options: { injectType: 'singletonStyleTag' },
            }, CSSLoader, postCSSLoader, "sass-loader"],
          },
          {
            test: /\.css$/,
            use: [
              "style-loader",
              {
                loader: "css-loader"
              },
              {
                loader: "postcss-loader",
                options: {
                  ident: "postcss",
                  plugins: () => [
                    require("postcss-flexbugs-fixes")
                  ]
                }
              }
            ]
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      react: path.resolve('./node_modules/react'),
      'react-router-dom': path.resolve('./node_modules/react-router-dom')
    }
  },
  performance: {
    hints: false
  }
};
