import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import VideoScroll from './components/videoScroll/VideoScroll';
import VideoScrollHidden from './components/videoScrollHidden/VideoScrollHidden';

import './styles/common.scss';

const App = () => {
	return (
		<BrowserRouter>
			<header>

			</header>
			<main>
				<Switch>
					<Route path='/1' exact render={() => <VideoScroll></VideoScroll>} />
					<Route path='/2' exact render={() => <VideoScrollHidden></VideoScrollHidden>} />
					<Route path='/3' exact render={() => <></>} />
				</Switch>
			</main>

			<footer>

			</footer>
		</BrowserRouter>
	)
}

export default App;