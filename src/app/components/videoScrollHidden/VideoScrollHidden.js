import React, { useState, useRef, useEffect } from 'react';

import './styles/VideoScrollHidden.scss';

/* Constante que determina el factor de división del video
*	Factor 100 => 1s por scroll
*  Factor 1000 => 0,1s por scroll
*/
const multiplier = 100;

const VideoScrollHidden = () => {
	const [frames, setFrames] = useState([]);
	const refCanvas = useRef(null);
	const refWrapper = useRef(null);

	useEffect(() => {
		const onScroll = event => {
			const scroll = event.target.scrollingElement.scrollTop;
			const frame = frames[Math.round(scroll / 100)];
			let img = document.createElement('img');
			img.src = frame;

			refCanvas.current.getContext('2d').drawImage(img, 0, 0);
		}

		window.addEventListener('scroll', onScroll)

		return () => {
			window.removeEventListener('scroll', onScroll)
		}
	})
	return (
		<div ref={refWrapper} styleName='wrapper'>
			<div styleName='videoContainer'>
				<canvas ref={refCanvas}></canvas>
				<video onLoadedData={event => changeHeight(event, refWrapper)} onTimeUpdate={event => timeUpdateHandler(event, frames, refCanvas, setFrames)} autoPlay src='./video/video.webm' controls={false} muted></video>
			</div>
		</div>
	)
}

const changeHeight = (event, refWrapper) => {
	event.preventDefault();

	//Altura calculada en función de la duración del vídeo y el multiplicador
	refWrapper.current.style.height = `${event.target.duration * multiplier + window.innerHeight}px`
}

const timeUpdateHandler = (event, frames, refCanvas, setFrames) => {
	event.preventDefault();

	let canvas = document.createElement('canvas');
	canvas.width = event.target.videoWidth;
	canvas.height = event.target.videoHeight;
	canvas.getContext('2d').drawImage(event.target, 0, 0, event.target.videoWidth, event.target.videoHeight);

	if (frames && frames.length === 0) {
		refCanvas.current.width = event.target.videoWidth;
		refCanvas.current.height = event.target.videoHeight;
		refCanvas.current.getContext('2d').drawImage(event.target, 0, 0, event.target.videoWidth, event.target.videoHeight);
	}

	setFrames([...frames, canvas.toDataURL()]);
}



export default VideoScrollHidden;