import React, { useRef, useEffect } from 'react';

import './styles/VideoScroll.scss';

/* Constante que determina el factor de división del video
*	Factor 100 => 1s por scroll
*  Factor 1000 => 0,1s por scroll
*/
const multiplier = 100;

const VideoScroll = () => {
	const refVideo = useRef(null);
	const refWrapper = useRef(null);

	useEffect(() => {
		const onScroll = event => {
			const scroll = event.target.scrollingElement.scrollTop;
			refVideo.current.currentTime = scroll / multiplier;
		}

		window.addEventListener('scroll', onScroll)

		return () => {
			window.removeEventListener('scroll', onScroll)
		}
	})

	return (
		<div ref={refWrapper} styleName='wrapper'>
			<div styleName='videoContainer'>
				<video ref={refVideo} onLoadedData={event => changeHeight(event, refWrapper)} src='./video/video.webm' controls={false}></video>
			</div>
		</div>
	)
}

const changeHeight = (event, refWrapper) => {
	event.preventDefault();

	//Altura calculada en función de la duración del vídeo y el multiplicador
	refWrapper.current.style.height = `${event.target.duration * multiplier + window.innerHeight}px`
}

export default VideoScroll;